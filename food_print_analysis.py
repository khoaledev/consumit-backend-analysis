from datetime import datetime
from utils.time import get_month_start
from model.food_print import FoodPrint


def update_wasted_food_print(user_id):
    """
    Update wasted food print of a user

    - Update last month food wasted first,
    since the system might miss the last day or system might down for some day of last month
    - For last month, only update if the record exist
    - For current month, create new food print record for this month if not existing
    - if newly created:
        ~ update monthly total, wasted, consumed
    - else:
        ~ update monthly wasted

    :param user_id:
    :return: FoodPrint object
    """

    last_month_food_print = FoodPrint.find(user_id=user_id, start_time=get_month_start(months_back=1))
    if last_month_food_print:
        last_month_food_print.update_monthly_wasted()

    food_print = FoodPrint.find_or_create(user_id=user_id, start_time=get_month_start())
    if food_print.is_newly_created():
        food_print.update_monthly_total()
        food_print.update_monthly_wasted()
        food_print.update_monthly_consumed()
    else:
        food_print.update_monthly_wasted()
    return food_print


def update_consumed_food_print(user_id, food_name, action_quantity, food_type, timestamp):
    """
    Update food consumed of a user

    - time stamp will be used to determine the date and the month a food item is updated
    - find or create the food print record if none exists
    - if created:
        ~ update monthly  total, wasted, and consumed
        ~ update the consumed with quantity 0 because monthly consumed already count this
    - else:
        ~update consumed for the food in food print

    :param user_id:
    :param food_name:
    :param action_quantity:
    :param food_type:
    :param timestamp:
    :return:
    """
    date = datetime.fromtimestamp(float(timestamp))

    food_print = FoodPrint.find_or_create(user_id=user_id, start_time=get_month_start(date=date))
    if food_print.is_newly_created():
        food_print.update_monthly_total()
        food_print.update_monthly_wasted()
        food_print.update_monthly_consumed()
        food_print.update_consumed(food_name=food_name, food_type=food_type, quantity=0, time=date)
    else:
        food_print.update_consumed(food_name=food_name, food_type=food_type, quantity=action_quantity, time=date)
    return food_print


def update_added_food_print(user_id, food_name, action_quantity, food_type, timestamp):
    """
    Update food added of a user

    - time stamp will be used to determine the date and the month a food item is updated
    - find or create the food print record if none exists
    - if created:
        ~ update monthly total, wasted, consumed
        ~ update added with quantity = 0, since monthly total already accounts for this new added.
    - else:
        ~ update added for the food in food print

    :param user_id:
    :param food_name:
    :param action_quantity:
    :param food_type:
    :param timestamp:
    :return:
    """
    date = datetime.fromtimestamp(float(timestamp))

    food_print = FoodPrint.find_or_create(
        user_id=user_id,
        start_time=get_month_start(date=date))
    if not food_print.get_last_update_total():
        food_print.update_monthly_total()
        food_print.update_monthly_wasted()
        food_print.update_monthly_consumed()
        food_print.update_added(food_name=food_name, food_type=food_type, quantity=0, time=date)
    else:
        food_print.update_added(food_name=food_name, food_type=food_type, quantity=action_quantity, time=date)
    return food_print


if __name__ == '__main__':
    user_id = '5a0baf55180916352c946b25'
    # user_id = '5a0bc3781809163a48ee7972'
    print(update_wasted_food_print(user_id=user_id))
    # print(update_consumed_food_print(user_id, 'potato', 10, 'vegetable', 1525528974.866607))
    # print(update_added_food_print(user_id, 'potato', 10, 'vegetable', 1525528974.466607))
