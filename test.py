import boto3
import config
import analysis
# SECRET_KEY = 'k0DLRHZ81r9PwOXtdO0O7fxUrwysnuR63o9aTD0V'
# ACCESS_KEY = 'AKIAJYQI523TE75EP75A'
# QUEUE_NAME = 'consumit_analytics'
# QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/194862814775/consumit_analytics'

sqs = boto3.client('sqs', aws_access_key_id=config.ACCESS_KEY, aws_secret_access_key=config.SECRET_KEY, region_name=config.REGION_NAME)


def main():
    while True:
        response = sqs.receive_message(
            QueueUrl=config.QUEUE_URL,
            WaitTimeSeconds=config.WAIT_TIME,
            MessageAttributeNames=['user_id'],
            VisibilityTimeout=config.VISIBILITY_TIMEOUT
        )
        print(response)
        messages = response.get('Messages', [])
        if messages:
            message = messages[0]
            mes_attr = message.get('MessageAttributes', {})
            user_id = mes_attr.get('user_id', {}).get('StringValue','')
            if user_id:
                analysis.analyze(user_id=user_id)
            # delete_queue(get_recipe_handle(message))
        # TODO: catch error


def get_recipe_handle(message):
    return message.get('ReceiptHandle', '')


def delete_queue(receipt_handle):
    sqs.delete_message(
                QueueUrl=config.QUEUE_URL,
                ReceiptHandle=receipt_handle
            )


if __name__ == '__main__':
    main()
