from utils.time import get_day_start, get_month_start
from .food_item import FoodItem
from bson import ObjectId
from .mongo_model import MongoModel
from pymongo import MongoClient, ReturnDocument
from datetime import datetime
from dateutil.relativedelta import relativedelta
from config import MONGODB_URI

client = MongoClient(MONGODB_URI, connectTimeoutMS=30000)
db = client.get_database()


class FoodPrint(MongoModel):
    """
    represent food print entry in analysis mongo collection
    """

    USER_ID = 'user_id'
    TYPE = 'type'
    GRANULARITY = 'granularity'
    START_TIME = 'start_time'
    LAST_UPDATE = 'last_update'
    DATA = 'data'
    LAST_UPDATE_TOTAL = 'last_update_total'
    LAST_UPDATE_WASTED = 'last_update_wasted'
    LAST_UPDATE_CONSUMED = 'last_update_consumed'
    CONSUMED_UPDATES = 'consumed_updates'
    ADDED_UPDATES = 'added_updates'

    @classmethod
    def new_record_json(cls, user_id, food_print_type='wasted_consumed', granularity='monthly', start_time=None):
        return {
            cls.USER_ID: ObjectId(user_id),
            cls.TYPE: food_print_type,
            cls.GRANULARITY: granularity,
            cls.START_TIME: start_time or get_month_start(),
        }

    @classmethod
    def find_by_id(cls, record_id):
        food_print = db.analysis.find_one({cls.MG_ID: ObjectId(record_id)})
        if not food_print:
            raise Exception('not found food print record with id {}'.format(record_id))
        return FoodPrint(food_print)

    @classmethod
    def find(cls, user_id, food_print_type='wasted_consumed', granularity='monthly', start_time=None):
        """
        find a food print record
        :param user_id:
        :param food_print_type:
        :param granularity:
        :param start_time:
        :return: FoodPrint object
        """
        food_print = db.analysis.find_one({
            cls.USER_ID: ObjectId(user_id),
            cls.TYPE: food_print_type,
            cls.GRANULARITY: granularity,
            cls.START_TIME: start_time or get_month_start(),
        })
        if not food_print:
            return None
        return FoodPrint(food_print)

    @classmethod
    def find_or_create(cls, user_id, food_print_type='wasted_consumed',
                       granularity='monthly', start_time=None):
        """
        find a food print record, if not found, create and insert a new record
        :param user_id:
        :param food_print_type:
        :param granularity:
        :param start_time:
        :return: FoodPrint object
        """
        food_print = cls.find(user_id, food_print_type, granularity, start_time)
        if not food_print:
            food_print = cls.create(user_id, food_print_type, granularity, start_time)
        return food_print

    @classmethod
    def create(cls, user_id, food_print_type='wasted_consumed', granularity='monthly', start_time=None):
        """
        create and insert a food print record
        :param user_id:
        :param food_print_type:
        :param granularity:
        :param start_time:
        :return: FoodPrint object
        """
        food_print = cls.new_record_json(user_id, food_print_type, granularity, start_time)
        db.analysis.insert_one(food_print)
        return FoodPrint(food_print)

    def update_monthly_total(self):
        """
        Update food total of this user for this month.

        - Current inventory of a month is all item quantity that had not been expired
        counting from the start of a month
        - Algorithm:
            + Get all un-expired food items from the start of this month
            + List through the list, get the quantity by the time (month start)
            + Add the derived quantity to current total of the food in data
            + Set attribute and save to db as the list is exhausted
                ~ to avoid non-idempotency, db updates only when last_update_total is none
                ~ if update succeed return self
                ~ else get latest version from db and cast to self

        :return: FoodPrint object with updated data
        """
        if self.get_last_update_total():
            return self

        month_start = self.get_start_time()
        food_items = FoodItem.get_inventory(getattr(self, FoodPrint.USER_ID), start=month_start)
        data = getattr(self, FoodPrint.DATA, {})
        for item in food_items:
            name = item.get_name()
            datum = data.get(name, FoodPrintDatum.new_item(name=name))
            datum.total = getattr(datum, FoodPrintDatum.TOTAL, 0) + item.get_quantity_by_time(month_start)
            datum.type = getattr(item, item.TYPE, 'undefined')
            data[name] = datum
        setattr(self, FoodPrint.DATA, data)
        setattr(self, FoodPrint.LAST_UPDATE_TOTAL, datetime.utcnow())

        rid = getattr(self, self.MG_ID, None)
        doc = self.to_dict(include=[self.DATA, self.LAST_UPDATE_TOTAL], exclude=[self.MG_ID])
        res = db.analysis.update_one({self.MG_ID: ObjectId(rid), self.LAST_UPDATE_TOTAL: None},
                                     {'$set': doc})
        if not res.matched_count:
            food_print = FoodPrint.find_by_id(rid)
            self.from_dict(food_print.to_dict())
        return self

    def update_monthly_wasted(self):
        """
        Update food wasted of this user for this month.

        - Support update food wasted for the whole month
        - A food is wasted when its expiration date is less than new day start.
        - Algorithm:
            + return if last wasted update is in the future =.=
            + return if the record is fully-monthly updated
            + if it is current month, get expired food item from last_wasted_update to today
            + else get expired food items for the rest last_wasted_update to next month start
            + if no food item expired during that period, update "last wasted update" and return
            + else iterate though items and add wasted amount to entry in food print data
            then save "last wasted update"

        :return: FoodPrint object with updated data
        """
        last_update_wasted = self.get_last_update_wasted() or self.get_start_time()
        today_start = get_day_start()
        next_month_start = self.get_start_time() + relativedelta(months=1)

        # if somehow last update is greater than today, return
        if last_update_wasted >= today_start:
            return self

        # if already update, return
        if last_update_wasted == next_month_start:
            return self

        # get expired items
        end = today_start
        if today_start > next_month_start:
            end = next_month_start
        food_items = FoodItem.get_expired_inventory(
            user_id=getattr(self, FoodItem.USER_ID),
            start=last_update_wasted,
            end=end)

        if not food_items:
            setattr(self, FoodPrint.LAST_UPDATE_WASTED, today_start)
            self.save(FoodPrint.LAST_UPDATE_WASTED)
            return self

        data = getattr(self, FoodPrint.DATA, {})
        for item in food_items:
            name = item.get_name()
            datum = data.get(name, FoodPrintDatum.new_item(name=name))
            datum.wasted = getattr(datum, FoodPrintDatum.WASTED, 0) + item.quantity
            data[name] = datum
        setattr(self, FoodPrint.DATA, data)
        setattr(self, FoodPrint.LAST_UPDATE_WASTED, today_start)
        self.save(FoodPrint.DATA, FoodPrint.LAST_UPDATE_WASTED)
        return self

    def update_monthly_consumed(self):
        """
        update current monthly consumed

        - This function is to recompute food consumed of the whole month till now.
        It is recommended that the function be run once when the record is created.
        Other subsequent consumption should be handle by update_consumed with each message.
        It required that the total and wasted are updated so that this formula can be useful:
            ~ consumed = total - left - wasted
        - Algorithm
            ~ return if last_update_consumed exist (update once already)
            ~ run update monthly total and monthly wasted to ensure data correctness
            ~ get all unexpired inventory from today and retrieve the total of quantity left
            ~ compute and assign to datum: consumed = total - left - wasted
        :return:
        """
        if get_month_start() > self.get_start_time():
            print('only support updating monthly consumed for current month')
            return self

        if self.get_last_update_consumed():
            return self

        self.update_monthly_total()
        self.update_monthly_wasted()

        # get current inventory
        day_start = get_day_start()
        food_items = FoodItem.get_inventory(getattr(self, FoodPrint.USER_ID), start=day_start)

        # compute the inv
        inventory = {}
        for item in food_items:
            inventory[item.get_name()] = inventory.get(item.get_name(), 0) + item.get_quantity()

        data = getattr(self, FoodPrint.DATA, {})
        for name, cur in inventory.items():
            datum = data.get(name, FoodPrintDatum.new_item(name=name))
            datum.consumed = getattr(datum, FoodPrintDatum.TOTAL, 0) - cur - getattr(datum, FoodPrintDatum.WASTED, 0)
            data[name] = datum

        setattr(self, FoodPrint.DATA, data)
        setattr(self, FoodPrint.LAST_UPDATE_CONSUMED, day_start)
        self.save(FoodPrint.DATA, FoodPrint.LAST_UPDATE_CONSUMED)

        return self

    def update_consumed(self, food_name, food_type, quantity, time):
        """
        update one consumed food for this record

        - To prevent non-idempotency, use a list as a time storage.
        Only update if the list does not have the time.
        - Use Mongo query to update
            + increase the consumed of a food in record data by the given quantity
            + store time in the list to avoid non-idempotency
            + set name and type
        - Since an consumed action might come before an add action of a new item,
        it is not guaranteed that this data entry has the name and type.
        Therefore, we need to set the name and type

        :param food_name:
        :param food_type:
        :param quantity:
        :param time:
        :return: FoodPrint object
        """
        res = db.analysis.find_one_and_update({
            self.MG_ID: ObjectId(getattr(self, self.MG_ID)),
            self.CONSUMED_UPDATES: {'$nin': [time]}
        }, {
            '$inc': {
                '{}.{}.{}'.format(FoodPrint.DATA, food_name, FoodPrintDatum.CONSUMED): float(quantity)
            },
            '$push': {
                FoodPrint.CONSUMED_UPDATES: time
            },
            '$set': {
                '{}.{}.{}'.format(FoodPrint.DATA, food_name, FoodPrintDatum.NAME): food_name.lower(),
                '{}.{}.{}'.format(FoodPrint.DATA, food_name, FoodPrintDatum.TYPE): food_type.lower()
            }
        },
            return_document=ReturnDocument.AFTER
        )

        if res:
            self.from_dict(res, include=[self.DATA, self.CONSUMED_UPDATES])

        return self

    def update_added(self, food_name, food_type, quantity, time):
        """
        update one added food for this record

        - To prevent non-idempotency, use a list as a time storage.
        Only update if the list does not have the time.
        - Use Mongo query to update
            + increase the total of a food in record data by the given quantity
            + store time in the list to avoid non-idempotency
            + set name and type
        - Since an added action can be a new item, we need to set name and type

        :param food_name:
        :param food_type:
        :param quantity:
        :param time:
        :return: FoodPrint object
        """
        res = db.analysis.find_one_and_update({
            self.MG_ID: ObjectId(getattr(self, self.MG_ID)),
            self.ADDED_UPDATES: {'$nin': [time]}
        }, {
            '$inc': {
                '{}.{}.{}'.format(FoodPrint.DATA, food_name, FoodPrintDatum.TOTAL): float(quantity)
            },
            '$push': {
                FoodPrint.ADDED_UPDATES: time
            },
            '$set': {
                '{}.{}.{}'.format(FoodPrint.DATA, food_name, FoodPrintDatum.NAME): food_name.lower(),
                '{}.{}.{}'.format(FoodPrint.DATA, food_name, FoodPrintDatum.TYPE): food_type.lower()
            }
        },
            return_document=ReturnDocument.AFTER
        )

        if res:
            self.from_dict(res, include=[self.DATA, self.ADDED_UPDATES])

        return self

    def is_newly_created(self):
        return not self.get_last_update_wasted() \
               and not self.get_last_update_total() \
               and not self.get_last_update_consumed()

    def get_start_time(self):
        return getattr(self, self.START_TIME)

    def get_last_update_wasted(self):
        return getattr(self, self.LAST_UPDATE_WASTED, None)

    def get_last_update_total(self):
        return getattr(self, self.LAST_UPDATE_TOTAL, None)

    def get_last_update_consumed(self):
        return getattr(self, self.LAST_UPDATE_CONSUMED, None)

    def save(self, *fields):
        """
        update the item, if item is not created (not having item id), not saving
        :param fields: fields to save
        :return:
        """
        iid = getattr(self, self.MG_ID, None)
        if not iid:
            return
        doc = self.to_dict(include=fields, exclude=[self.MG_ID])
        db.analysis.update_one({self.MG_ID: ObjectId(iid)},
                               {'$set': doc})

    def to_dict(self, include=(), exclude=(), deepcopy=False):
        """
        override to parse FoodPrintDatum

        :param include:
        :param exclude:
        :param deepcopy:
        :return:
        """
        d = super().to_dict(include, exclude, deepcopy=True)
        if self.DATA in d:
            data = d.get(self.DATA)
            for key, datum in data.items():
                data[key] = datum.to_dict()
            d[self.DATA] = data
        return d

    def from_dict(self, doc, include=(), exclude=()):
        """
        override to parse FoodPrintDatum

        :param doc:
        :param include:
        :param exclude:
        :return:
        """
        super().from_dict(doc, include, exclude)

        keys = [k for k in include if k not in exclude]
        if not keys or self.DATA in keys:
            data = getattr(self, FoodPrint.DATA, {})
            for key, value in data.items():
                data[key] = FoodPrintDatum(value)
            setattr(self, FoodPrint.DATA, data)


class FoodPrintDatum(MongoModel):
    """
    Represent an item of data in FoodPrint's data attribute
    """
    NAME = 'name'
    TOTAL = 'total'
    CONSUMED = 'consumed'
    WASTED = 'wasted'
    TYPE = 'type'

    @classmethod
    def new_item_json(cls, name, wasted=0, consumed=0, total=0, quantity_type=''):
        return {
            FoodPrintDatum.NAME: name,
            FoodPrintDatum.TOTAL: total,
            FoodPrintDatum.CONSUMED: consumed,
            FoodPrintDatum.WASTED: wasted,
            FoodPrintDatum.TYPE: quantity_type}

    @classmethod
    def new_item(cls, name, wasted=0, consumed=0, total=0, quantity_type=''):
        return FoodPrintDatum(cls.new_item_json(name, wasted, consumed, total, quantity_type))
