from utils.time import get_day_start
from bson import ObjectId
from .mongo_model import MongoModel
from pymongo import MongoClient
from config import MONGODB_URI

client = MongoClient(MONGODB_URI, connectTimeoutMS=30000)
db = client.get_database()


class FoodItem(MongoModel):
    """
    representing food item in food item mongo collection
    """
    EXPIRY_EXPIRED = 'expired'
    EXPIRY_CONSUMED = 'consumed'
    EXPIRY_BOTH = 'both'

    USER_ID = 'user_id'
    NAME = 'name'
    TYPE = 'type'
    QUANTITY = 'quantity'
    QNT_TYPE = 'quantity_type'
    INPUT_DATE = 'input_date'
    EXP_DATE = 'exp_date'
    EXP_DAYS = 'exp_days'
    IS_EXPIRED = 'is_expired'
    IS_CONSUMED = 'is_consumed'
    CONSUMPTION = 'consumption'
    LAST_UPDATE = 'last_update'
    DAYS_LEFT = 'days_left'
    HOURS_LEFT = 'hours_left'

    _INIT_FIELDS = (NAME, TYPE, EXP_DAYS, QUANTITY, QNT_TYPE)

    @classmethod
    def get_current_inventory(cls, user_id, start=None):
        """
        current inventory are items those not expired and not consumed
        :param user_id:
        :param start:
        :return:
        """
        res = db.food_items.find({
            FoodItem.USER_ID: ObjectId(user_id),
            FoodItem.EXP_DATE: {'$gte': start or get_day_start()},
            FoodItem.IS_CONSUMED: False
        })
        return [FoodItem(item) for item in res]

    @classmethod
    def get_inventory(cls, user_id, start=None):
        """
        normal inventory are those not expired
        :param user_id:
        :param start:
        :return:
        """
        res = db.food_items.find({
            FoodItem.USER_ID: ObjectId(user_id),
            FoodItem.EXP_DATE: {'$gte': start or get_day_start()}
        })
        return [FoodItem(item) for item in res]

    @classmethod
    def get_expired_inventory(cls, user_id, start, end):
        """
        expired inventory are those expired
        :param user_id:
        :param start:
        :param end:
        :return:
        """
        res = db.food_items.find({
            FoodItem.USER_ID: ObjectId(user_id),
            FoodItem.EXP_DATE: {
                '$gte': start,
                '$lt': end
            },
            FoodItem.IS_CONSUMED: False
        })
        return [FoodItem(item) for item in res]

    def _filter_consumption_by_time(self, start):
        """
        filter and sort consumption down to those having update time after given start time
        :param start: start time
        :return: array of consumption record
        """
        consumption = [c for c in getattr(self, self.CONSUMPTION) if c['timestamp'] >= start]
        consumption = sorted(consumption, key=lambda c: c['timestamp'])
        return consumption

    def get_quantity_by_time(self, time):
        """
        return the quantity at a given time

        - Algorithm:
            + filter and sort consumption with update time being after the given time
            + if there is result (there is at least one update after the time),
            use the 1st entry of the filtered consumption
                ~ if that's an input action, the just use the entry quantity
                ~ if that's an eat action, then use the sum of the amount and quantity left
            + elif there is no result (last update happen before time),
            use the last entry of full list consumption
                ~ does not matter if it is input or eat, the quantity is what's left
                ~ does not matter if it is consumed or not, the quantity is what's left
                ~ use the entry quantity

        :param time:
        :return:
        """
        consumption = self._filter_consumption_by_time(start=time)

        if consumption:
            # in case we have updates after the given time, we will have filtered consumption
            entry = consumption[0]
            if entry['action'] == 'input':
                quantity = entry['quantity']
            else:
                quantity = entry['quantity'] + entry['amount']
        else:
            # in case no action after given time, get last element of consumption record
            entry = getattr(self, self.CONSUMPTION)[-1]
            quantity = entry['quantity']
        return quantity

    def get_name(self):
        return getattr(self, self.NAME, 'undefined').lower()

    def get_quantity(self):
        """return current quantity"""
        return getattr(self, self.QUANTITY, 0)
