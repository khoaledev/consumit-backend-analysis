from datetime import datetime, timedelta


def get_month_start(months_back=0, date=None):
    """
    get the start of the current month. can also get the start of a previous month 
    by setting the value months_back.
    @params: months_back: default to 0, meaning current month
    """
    if not date:
        date = datetime.utcnow()
    month_start = datetime(date.year, date.month, 1)
    for _ in range(months_back):
        last_month_last_date = month_start - timedelta(days=1)
        month_start = datetime(last_month_last_date.year, last_month_last_date.month, 1)
    return month_start


def get_day_start(days_back=0, date=None):
    """
    get the start of today. can also get the start of a previous day 
    by setting the value days_back.
    @params: days_back: default to 0, meaning today
    """
    if not date:
        date = datetime.utcnow()
    day_start = datetime(date.year, date.month, date.day)
    day_start = day_start - timedelta(days=days_back)
    return day_start
