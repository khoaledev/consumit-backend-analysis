import boto3
import config
import analysis
import food_print_analysis as fpa
import sys
import traceback


sqs = boto3.client(
    'sqs',
    aws_access_key_id=config.ACCESS_KEY,
    aws_secret_access_key=config.SECRET_KEY,
    region_name=config.REGION_NAME)


FOOD_ANALYSIS_MSG = 'food_analysis'
FOOD_CONSUMED_MSG = 'consumed_food'
FOOD_ADDED_MSG = 'added_food'


def main():
    """
    - Run forever or until the server is shutdown
    - Check and retrieve message periodically (every 10 seconds)
    - Read message title and run a task appropriately with message attributes
    - Block message for a certain time so that no other consumer node can do the same job
    - Delete message when success/failure
    :return:
    """
    while True:
        # get the message every 10 seconds
        response = sqs.receive_message(
            QueueUrl=config.QUEUE_URL,
            WaitTimeSeconds=config.WAIT_TIME,
            MessageAttributeNames=['user_id', 'food_name', 'quantity', 'update_timestamp', 'food_type'],
            VisibilityTimeout=config.VISIBILITY_TIMEOUT
        )
        messages = response.get('Messages', [])

        for message in messages:
            try:
                # direct to appropriate handler
                print()
                print('------------------------------------------------')
                print(message)
                print("message: " + message['Body'])
                print(message.get('MessageAttributes', {}))

                if message['Body'] == FOOD_ANALYSIS_MSG:
                    mes_attr = message.get('MessageAttributes', {})
                    user_id = mes_attr.get('user_id', {}).get('StringValue', '')
                    if user_id:
                        analysis.analyze_user_history(user_id=user_id)
                        fpa.update_wasted_food_print(user_id=user_id)
                elif message['Body'] == FOOD_CONSUMED_MSG:
                    mes_attr = message.get('MessageAttributes', {})
                    fpa.update_consumed_food_print(
                        user_id=mes_attr.get('user_id', {}).get('StringValue', ''),
                        food_name=mes_attr.get('food_name', {}).get('StringValue', ''),
                        action_quantity=mes_attr.get('quantity', {}).get('StringValue', ''),
                        food_type=mes_attr.get('food_type', {}).get('StringValue', ''),
                        timestamp=mes_attr.get('update_timestamp', {}).get('StringValue', '')
                    )
                elif message['Body'] == FOOD_ADDED_MSG:
                    mes_attr = message.get('MessageAttributes', {})
                    fpa.update_added_food_print(
                        user_id=mes_attr.get('user_id', {}).get('StringValue', ''),
                        food_name=mes_attr.get('food_name', {}).get('StringValue', ''),
                        action_quantity=mes_attr.get('quantity', {}).get('StringValue', ''),
                        food_type=mes_attr.get('food_type', {}).get('StringValue', ''),
                        timestamp=mes_attr.get('update_timestamp', {}).get('StringValue', ''),
                    )
            except Exception:
                # print trace for debug
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_exception(exc_type, exc_value, exc_traceback, file=sys.stdout)
            finally:
                # delete message even if it fail
                delete_message(get_recipe_handle(message))
        

def get_recipe_handle(message):
    return message.get('ReceiptHandle', '')


def delete_message(receipt_handle):
    sqs.delete_message(
                QueueUrl=config.QUEUE_URL,
                ReceiptHandle=receipt_handle
            )


if __name__ == '__main__':
    main()
